---
source: Decentrala
section: 6
title: Soft-Serve Webhooks
---

### Soft serve webhooks

Soft serve supports [webhooks](https://en.wikipedia.org/wiki/Webhook)

Webhooks allow us to get notified on some web server when some events occur on the soft serve git server. Currently supported events that can be subscribed to (for a specific repo) are:

- branch_tag_create
- branch_tag_delete
- collaborator
- push
- repository
- repository_visibility_change

See help page for repo webhooks
`ssh -p 2222 soft.dmz.rs repo webhook --help`

##### Example

An example for using a webhook could be that you want to be notified when anyone pushes commits to `boban` repo
You could create a webhook that will trigger on the push event of the boban repo and send a request to boban.dmz.rs/push url (where you could host a webapp that listens for that request). The webapp will react to it by sending a xmpp message with details on which commits where pushed etc.
`webhook create boban boban.dmz.rs/push --events push`
