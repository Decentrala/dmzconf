---
source: Decentrala
section: 6
title: Soft-Serve Basics
---

Soft Serve has its configurations stored inside itself in a repo.  Admins can pull:

`git clone ssh://soft.dmz.rs:2222/.soft-serve`

# Adding Users

Summary:

`ssh -p 2222 soft.dmz.rs user --help`

Add user `ana` to the `fixme` repository:

```bash
user=ana
repo=fixme
ssh -p 2222 soft.dmz.rs user create $user
key="$(cat ~/dmzadmin/ssh_keys/alice.pub)"
ssh -p 2222 soft.dmz.rs user add-pubkey "$key" $user
ssh -p 2222 soft.dmz.rs repo collab add $repo $user
ssh -p 2222 soft.dmz.rs user info $user
```

Add `bojan` as an admin (who can see an change all repositories):


```bash
user=bojan
sshkey="ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIF5g6oP6+DyFhkIrN4pRcvsQ7RgNavEyzN2kH8yOB6mA bojan@posteo.net"
ssh -p 2222 soft.dmz.rs user create --admin --key "$sshkey" "$user"
```
