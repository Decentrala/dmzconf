# This make file produces the smtp daemon for the current backup domain: splint.rs

# It is missing the cert, so you'll have to make another.
# It hasn't been tested in a few years, and I have no idea how to make a
# containerized test which will check DNS, and SSL certificates.

DOMAIN=splint.rs

output: service

/usr/bin/smtp:
	pacman -S smtpd
	cp etc/smtpd/* /etc/smtpd
	smtpd -n

/etc/smtpd/mailname: /usr/bin/smtp
	echo $(DOMAIN) > /etc/smtpd/mailname

.PHONY: service
service: /etc/systemd/system/multi-user.target.wants/smtpd.service
/etc/systemd/system/multi-user.target.wants/smtpd.service: /etc/smtpd/mailname
	systemctl enable --now smtpd
