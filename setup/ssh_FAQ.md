---
volume: Decentrala
section: 6
title: ssh setup
author: Malin
source: dmz.rs
---

## Step 1: Basic `ssh`

> I did stuff with my `ssh` and now things don't work. What do?

Check the permissions on your `ssh` directory:

```bash
$ ls -d ~/.ssh
drwxr-x--- - ghost  3 Dec 12:55 /home/ghost/.ssh
```

This is wrong, because anyone in your `~` can see you `ssh` configuration files.

```bash
$ chmod -R 600 ~/.ssh
$ ls -d ~/.ssh
drw------- - ghost  3 Dec 12:55 /home/ghost/.ssh
```

This is also wrong - entering a directory is the same as executing it.
If you can't 'execute' the directory, you cannot enter it, and `ssh` cannot read the files.

```bash
$ chmod -R 700 ~/.ssh
$ ls -l ~/.config

-rwx------ 1 ghost dmz   578 Dec 27  2022 authorized hosts
-rwx------ 1 ghost dmz  1145 Dec 27  2022 authorized keys
-rwx------ 2 ghost dmz   366 Dec 14 18:36 config
-rwx------ 1 ghost dmz   419 Dec 11  2023 id ed25519
-rwx------ 1 ghost dmz   106 Dec 11  2023 id ed25519.pub
-rwx------ 1 ghost dmz  2610 Dec 27  2022 id rsa
-rwx------ 1 ghost dmz   578 Dec 27  2022 id rsa.pub
-rwx------ 1 ghost dmz 28269 Dec 28 17:32 known hosts
```

Now all the files have 'read, write, and execute', but only for `$USER`.

## Step 2: The Config File

> I have 43 different `ssh` keys.  Something doesn't work with a program.  What do?

- Option 1: Delete all of them and stop asking Santa for `ssh` keys.
- Option 2: Define which one you want to use in the `~/.ssh/config` file.


```
Host soft
    HostName soft.dmz.rs
    Port 2222
	User ghost
	IdentityFile ~/.ssh/id rsa
Host dmz
    HostName dmz.rs
    Port 123
    User root
Host krov
	HostName dmz.rs
	Port 5555
	User ghost
Host june
	HostName 192.168.1.100
	User ghost
	ProxyJump krov
```


The first example lets you go to the `soft-serve` git-server just by typing

```bash
$ ssh soft
```

If you're not sure if ssh is using the right key, try with `-v` for 'verbose mode'.

```bash
$ ssh -vv soft
```

If you're not sure if ssh is using the right key, try with `-v` for 'verbose mode'.

> `git` is not working with `ssh`

`git` will not presume to use your `ssh` config file unless you tell it:

```bash
$ GIT_SSH_COMMAND="ssh -F ~/.ssh/config" git pull
```

If that works, you can make the change permanent for that one repository:


```bash
$ git config core.sshCommand "ssh -F ~/.ssh/config"
```

