---
volume: Decentrala
section: 6
title: git aliases
author: Malin
source: dmz.rs
---

## Aliases

Put these in `~/.bash_aliases`

```
alias gb='git branch'
alias gc='git add -p . && git commit'
alias gd="git diff --word-diff"
alias gl='git log --graph --show-signature'
alias gla="git log --all --decorate --oneline --graph"
alias gm='git merge'
alias gis='git status'
```

## Dangerous Aliases

Get a fuzzy-finder, like `fzy,` or `sk` (called `sk-im` in the repos), and checkout faster:

```
alias gco='git checkout --recurse-submodules $(sk -c "git branch | cut -c 3-")'
```

Delete all changes and start again instantly:

```
alias grs='git reset --hard HEAD'
```

Push to remotes that don't use http:

```
alias gpa='git remote show  | while read remote; do git remote get-url $remote | grep -qv http && git push $remote; done'
```
