These setup files provide the text-only configurations for DMZ.

*It should not contain private data.*

# Aspirations

- Each service should reside in its own directory.
- Everything should be automated, including:
    - backups,
    - turning the backup back into a service,
    - configuring the service to run,
    - Makefile (or similar) wherever practical,
    - Idempotency.
- All secrets stored elsewhere (probably in the `dmzadmin` repo)
- Any maintenance scripts.
- Configurations should reside in shadow-directories, e.g. a backup `soft-serve`'s `config.yaml` should reside in this repo under `splint.rs/soft-serve/etc/soft/config.yaml`.

