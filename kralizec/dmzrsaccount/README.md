install luser.deb
change /var/luser/luser/config.ini

add ldap.krov.dmz.rs to /etc/hosts with the IP address op slapd vm by adding a line like "192.168.1.205 ldap.krov.dmz.rs"

config.ini should contain following:

LDAPHOST = ldap.krov.dmz.rs
LDAPADMINNAME = cn=admin,dc=dmz,dc=rs
LDAPPASS = <admin password set during installation of slapd program and dpkg-reconfigure on slapd vm>
USERBASE = ou=Users,dc=dmz,dc=rs

run prepare.py
