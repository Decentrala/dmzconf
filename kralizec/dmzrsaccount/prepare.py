#!/usr/bin/env python3
import ldap3
import configparser

CONFIG_PATH = "/var/luser/luser/config.ini"
config = configparser.ConfigParser()
config.read(CONFIG_PATH)
LDAPHOST = config.get('credentials', 'LDAPHOST')
LDAPADMINNAME = config.get('credentials', 'LDAPADMINNAME')
LDAPPASS = config.get('credentials', 'LDAPPASS')
USERBASE = config.get('credentials', 'USERBASE')

ldapserver=ldap3.Server(LDAPHOST,use_ssl=True)
ldapconnection = ldap3.Connection(ldapserver, LDAPADMINNAME, LDAPPASS, auto_bind=True)
rcode1=ldapconnection.add(f'{USERBASE}', ['dcObject', 'organization'], {'o' : "dmz", 'dc' : "dmz"})
rcode2=ldapconnection.add(USERBASE, ['top', 'organizationalUnit'], {'ou' : "Users"})
print(str(rcode1))
print(str(rcode2))
