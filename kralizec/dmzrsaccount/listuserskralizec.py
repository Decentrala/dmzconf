#!/usr/bin/env python3
import ldap3

LDAPADMINNAME='uid=krovslapd,ou=xmpp,dc=dmz,dc=rs'
LDAPPASS='<krovslapd password>'
USERATTRIBUTES=['cn' , 'sn', 'givenName', 'uid', 'uidNumber' , 'gidNumber', 'homeDirectory', 'loginShell', 'gecos' , 'shadowLastChange', 'shadowMax', 'userPassword', 'mail']


ldapserver=ldap3.Server('2001:470:1f1a:1a4:0:1:0:1d',use_ssl=True)
ldapconnection = ldap3.Connection(ldapserver, LDAPADMINNAME,LDAPPASS, auto_bind=True)
ldapconnection.search(search_base=f'ou=xmpp,dc=dmz,dc=rs',search_filter='(objectClass=person)', attributes=USERATTRIBUTES)
print(str(ldapconnection.response))
