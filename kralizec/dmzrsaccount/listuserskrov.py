#!/usr/bin/env python3
import ldap3
import configparser

CONFIG_PATH = '/var/luser/luser/config.ini'
config = configparser.ConfigParser()
config.read(CONFIG_PATH)
LDAPHOST = config.get('credentials', 'LDAPHOST')
LDAPADMINNAME = config.get('credentials', 'LDAPADMINNAME')
LDAPPASS = config.get('credentials', 'LDAPPASS')
USERBASE = config.get('credentials', 'USERBASE')
USERATTRIBUTES=['cn' , 'sn', 'givenName', 'uid', 'uidNumber' , 'gidNumber', 'homeDirectory', 'loginShell', 'gecos' , 'shadowLastChange', 'shadowMax', 'userPassword', 'mail']

ldapserver=ldap3.Server(LDAPHOST,use_ssl=True)
ldapconnection = ldap3.Connection(ldapserver, LDAPADMINNAME,LDAPPASS, auto_bind=True)
ldapconnection.search(search_base=f'{USERBASE}',search_filter='(objectClass=person)', attributes=USERATTRIBUTES)
print(str(ldapconnection.response))
