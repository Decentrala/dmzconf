---
title: ejabberd configurations
section: 6
source: Decentrala
---

#On your PC
Add this configuration to ~/.ssh/config

Host dmzkrovejabberd12
  Hostname zd4bzozu3uapjpqftoux66l22kfyju7bkxnooefqia3lp7hplg3ayiid.onion
  User root
  IdentityFile ~/.ssh/id_rsa
  PasswordAuthentication no

Now you can log in by typing:
torsocks ssh dmzkrovejabberd12

#On the server:
Add to /etc/hosts file
192.168.1.209 sql.krov.dmz.rs 
192.168.1.205 ldap.krov.dmz.rs 

Copy cert directory from dmzkrovsshfs12:/var/shareddirs/nginx12ejabberd12/xmpp.krov.dmz.rs to /etc/ssl/certs/ 

set permissions (set gid bit) for /etc/ssl/certs directory
You can do this by typing:
chown 2770 /etc/ssl/certs

Add to crontab with ("crontab -e") commands to copy with scp certificates the domain from dmzkrovsshfs12
0 1 * * * /usr/bin/scp -r dmzkrovsshfs12:/var/shareddirs/nginx12ejabberd12/xmpp.krov.dmz.rs /etc/ssl/certs/
1 1 * * * /usr/bin/chmod 600 /etc/ssl/certs/xmpp.krov.dmz.rs/privkey.pem
2 1 * * * /usr/sbin/ejabberdctl restart

Generate dhparm keys with:
openssl dhparam -out /etc/ejabberd/dhparams.pem 2048

Copy ejabberd.yml to /etc/ejabberd/
ejabberdctl restart
