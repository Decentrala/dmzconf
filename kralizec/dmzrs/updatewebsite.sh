#!/bin/bash
cd /root/website
/usr/bin/git restore .
/usr/bin/git pull
/usr/bin/python3 prep.py
/usr/bin/python3 build_pages.py
/usr/bin/rsync -a --delete /root/website/site/* /var/www/dmzrs/
