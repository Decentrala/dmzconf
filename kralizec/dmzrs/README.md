Add this configuration to `~/.ssh/config` file

```
Host dmzkrovdmzrs12
  Hostname veyxphzuqnooc7wb7utfza3joaoopgqgwp6l6d4en5yfmyr7kxvminqd.onion
  User root 
  IdentityFile ~/.ssh/id_rsa
  PasswordAuthentication no

```

Now you can log in by typing:


```bash
torsocks ssh dmzkrovdmzrs12
```

Install all needed packages:


```bash
apt install rsync git nginx
git clone https://gitea.dmz.rs/Decentrala/website
```

Run `updatewebsite.sh` script every minute using `crontab` (run "`crontab -e`")
This fill automatically pull from git repo and regenerate events page

Add `nginx-dmz.rs.conf` to `/etc/nginx/sites-available/dmz.rs` and create a symlink
from `/etc/nginx/sites-enabled/dmz.rs` to that file.
You can do this by running:

```bash
ln -s /etc/nginx/sites-available/dmz.rs /etc/nginx/sites-enabled/dmz.rs
```

Increase `server_names_hash_bucket_size` to 256 in `/etc/nginx/nginx.conf` in order to support onion addresses.

In the `nginx` configuration /account/ is redirected to the `luser` [instance](https://gitea.dmz.rs/fram3d/luser)  running at `192.168.1.211`.
