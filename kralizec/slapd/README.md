Create ldap users at dmz.rs/account for users in the servicesaccounts.txt
these accounts should be listed in /root/ldifs/addacl.ldif
to generate addacl.ldif run generateacl.sh

add tls keys in /etc/ssl/certs/ldap.krov.dmz.rs

# Generate password for admin user on this server only and add it when asked during installation
apt install slapd

# For domain set dmz.rs for Organization set Users for admin password use previously generated password
dpkg-reconfigure slapd

# change /etc/default/slapd to replace ldap:// with ldaps:// under SLAPD_SERVICES
service slapd restart
./setup.sh

dmzrsaccount vm should run prepare.py 
ldapsync vm should run sync.py
