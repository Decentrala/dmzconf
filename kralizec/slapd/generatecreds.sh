#!/bin/bash

function genpass(){
echo $(shuf ../../scripts/shared/english.txt | head) | sed "s/ //g"
}

for i in $(cat servicesaccounts.txt) ; do printf "%s\n" $(echo -n $i | cut -d"," -f 1 | cut -d"=" -f2 ; genpass) | gpg -e -r fram3d@dmz.rs -r sienna@dmz.rs --output creds/$(echo -n $i | cut -d"," -f 1 | cut -d"=" -f2).gpg ; done

