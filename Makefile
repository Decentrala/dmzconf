.PHONY: help

help: ## Print the help message
	@awk 'BEGIN {FS = ":.*?## "} /^[0-9a-zA-Z._-]+:.*?## / {printf "\033[36m%s\033[0m : %s\n", $$1, $$2}' $(MAKEFILE_LIST) | \
		sort | \
		column -s ':' -t

map.txt: map.ge ## Making map.txt
	grep -v '# unimportant' $< | graph-easy --boxart > $@
	cat $@

full_map.txt: map.ge ## Generating full_map.txt with graph-easy
	graph-easy --boxart < $< > $@
	cat $@

########## Man Pages ##########

mandir = $(HOME)/.local/man/man6

kralizec_docs != grep -rl "^section:" kralizec 
kralmans = $(kralizec_docs:kralizec/%/README.md=$(mandir)/%.6)

$(mandir)/%.6: kralizec/%/README.md
	lowdown -stman $< > $@

krov_docs != grep -rl "^section:" krov 
krovmans = $(krov_docs:krov/%/README.md=$(mandir)/%.6)

$(mandir)/%.6: krov/%/README.md
	lowdown -stman $< > $@

splint_docs != grep -rl "^section:" splintrs
splintmans = $(splint_docs:splintrs/%/README.md=$(mandir)/%.6)

$(mandir)/%.6: splintrs/%/README.md
	lowdown -stman $< > $@

setup_docs != grep -rl "^section:" setup 
setupmans = $(setup_docs:setup/%.md=$(mandir)/%.6)

$(mandir)/%.6: setup/%.md
	lowdown -stman $< > $@

$(mandir):
	mkdir -p $@

$(kralmans) $(krovmans) $(splintmans) $(setupmans) :| $(mandir)

.PHONY: pages
pages: $(kralmans) $(krovmans) $(setupmans) $(splintmans)
	$(info $(kralmans))
	@test ! $(command -v mandb) || mandb --user-db
	$(info Open DMZ's man pages with 'man 6 <tab>')

##########

clean:
	$(RM) $(kralmans) $(krovmans)
