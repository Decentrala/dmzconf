#!/bin/bash
# This script needs ssh access to the gitsync repo.
# Since gitsync is not exposed, you must jump through a middle-man which is exposed.
# In my case, this is my machine 'nimbus', but you should set it to be any machine on the internal network.

middleMan=nimbus


# Try this first line on its own if it's not obvious what's happening.
ssh -J $middleMan gitsync@192.168.1.36 "cd /var/gitsyncrepos/ && ls *" | while read -r line; do
	if [ "$(echo $line | grep ':')" ]; then
		owner="$(echo $line | sed 's/://')"
	elif [ "$(echo $line | grep '.git')" ]; then
		repo=$(echo $line | sed 's/.git//g') && \
		echo "ssh -p 2222 soft.dmz.rs repo description $repo" | sh || \
		# you can tell soft-serve to mirror a repo with one command:
		echo "ssh -p 2222 soft.dmz.rs repo import $repo https://gitea.dmz.rs/$owner/$repo -m " | sh
	fi
done
