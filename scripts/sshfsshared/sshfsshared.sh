#!/bin/bash
## Add accounts and shared folder for two users given as two arguments $1 and $2

DMZKEYFILE=$(echo -n ~)"/.ssh/dmz"

USER1=$1
USER2=$2
HOST="$3"

if [[ -z "$USER1" ]]; then 
	echo "Set USER1"
        exit 1
fi

if [[ -z "$USER2" ]]; then 
	echo "Set USER2"
        exit 1
fi

if [[ -z "$HOST" ]]; then 
	HOST="dmzkrovsshfs12"
fi

ssh-add -t 100 $DMZKEYFILE
torsocks ssh $HOST "adduser $USER1 ; adduser $USER2 ; groupadd $USER1$USER2 ; adduser $USER2 $USER1$USER2 ; adduser $USER1 $USER1$USER2 ; mkdir -p /var/shareddirs/$USER1$USER2 ; chown $USER1:$USER1$USER2 /var/shareddirs/$USER1$USER2 ; chmod 2770 /var/shareddirs/$USER1$USER2 "
