#!/bin/bash
## Install luser.deb to remote host

DMZKEYFILE=$(echo -n ~)"/.ssh/dmz"

HOST="$1"
DEB="$2"

if [[ -z "$HOST" ]]; then 
	echo "Set Luser Host"
	echo "Usage: ./installdeb.sh HOSTNAME DEB_FILE"
        exit 1
fi

if [[ -z "$DEB" ]]; then 
	echo "Set Deb package name"
	echo "Usage: ./installdeb.sh HOSTNAME DEB_FILE"
        exit 1
fi

cd ~/src/$DEB/build-deb
make
ssh-add -t 100 $DMZKEYFILE
torsocks scp $DEB.deb $HOST:~/
torsocks ssh $HOST "apt install -y /root/$DEB.deb ; rm /root/$DEB.deb"
