#!/bin/bash
## Generate TLS certificate for a new subdomain

DMZKEYFILE=$(echo -n ~)"/.ssh/dmz"

NGINXHOST=$1
SUBDOMAIN=$2
CTIPADDRESS=$3

if [[ -z "$NGINXHOST" ]]; then 
	echo "Set Nginx Host"
        exit 1
fi

if [[ -z "$SUBDOMAIN" ]]; then 
	echo "Set subdomain"
        exit 1
fi


ssh-add -t 200 $DMZKEYFILE
torsocks scp ./remotecmd $NGINXHOST:/root/remotecmd.sh
torsocks scp ./subdomain.dmz.rs.http $NGINXHOST:/etc/nginx/sites-available/
torsocks scp ./subdomain.dmz.rs.https $NGINXHOST:/etc/nginx/sites-available/

if [[ -z "$CTIPADDRESS" ]]; then 
	torsocks scp ./subdomain.dmz.rs.proxy $NGINXHOST:/etc/nginx/sites-available/
fi

torsocks ssh $NGINXHOST "chmod +x /root/remotecmd.sh; /root/remotecmd.sh $SUBDOMAIN"

