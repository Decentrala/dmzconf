#!/bin/bash
## Create new Linux contanier at a proxmox host

DMZKEYFILE=$(echo -n ~)"/.ssh/dmz"

TORHOST=$1
CTHOST=$2
CTIP=$3

if [[ -z "$TORHOST" ]]; then 
	echo "Set Tor Host"
        exit 1
fi

if [[ -z "$CTHOST" ]]; then 
	echo "Set CT hostname"
        exit 1
fi

if [[ -z "$CTIP" ]]; then 
	CTIP=$(grep -C 1 $CTHOST ~/.ssh/autoconfig | tail -1 | cut -d" " -f4)
fi

ssh-add -t 600 $DMZKEYFILE
torsocks scp ./remotecmd $TORHOST:/root/onionadd.sh

torsocks ssh $TORHOST "chmod +x /root/onionadd.sh; /root/onionadd.sh $CTHOST $CTIP" >> cmdoutput.txt
torsocks ssh $TORHOST "rm /root/onionadd.sh"

HOSTNAME=$(tail -1 cmdoutput.txt)

echo "Host "$CTHOST"onion" >> ~/.ssh/autoconfig
echo "  Hostname "$HOSTNAME >> ~/.ssh/autoconfig
echo "  User root" >> ~/.ssh/autoconfig
echo "  IdentityFile "$DMZKEYFILE >> ~/.ssh/autoconfig
echo "  PasswordAuthentication no" >> ~/.ssh/autoconfig
