## ssh11

- Function: used to access other machines at kralizec.
- Access: `ssh -l $USER rmvm4vrp352yhgtr73w5nafbrtsrsdgj2x7jolpy7b4czqxstt5abfid.onion`
- IP: 192.168.1.20

## gitea11

- Function: gitea.dmz.rs
- Access: `ssh -p 2222 root@dmz.rs`
- IP: 192.168.1.36

## http11

- Function: dmz.rs website (nginx)
- Access:
    1. Start `tor` service.
    2. `torsocks ssh -J $USER@rmvm4vrp352yhgtr73w5nafbrtsrsdgj2x7jolpy7b4czqxstt5abfid.onion root@192.168.1.41`
- IP: 192.168.1.41
